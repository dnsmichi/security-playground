// requires GPLv2
// reverse dependency to a crypto CVE in https://github.com/prometheus/client_golang/issues/649
module gitlab.com/dnsmichi/security-playground

go 1.14

require (
	github.com/vjeantet/ldapserver v1.0.1
	github.com/prometheus/common v0.6.0
)

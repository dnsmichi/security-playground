# security-playground

This is a playground for GitLab's [Application Security features](https://docs.gitlab.com/ee/user/application_security/).

## SAST

- [c/main.c](c/main.c) uses unsafe memory copying

## Container Scanning

- [Dockerfile](Dockerfile) uses `debian:10.0` which has known vulnerabilities

## Dependency Scanning

- [go/go.sum](go/go.sum) pulls `prometheus/common` which pulls in vulnerable reverse dependencies
- [python/requirements.txt](python/requirements.txt) pulls `django` with known CVEs.
- [ruby/Gemfile](ruby/Gemfile) pulls `json` with known CVEs.

## Secrets Scanning

- [ruby/main.rb](ruby/main.rb) hardcodes an AWS key
- [.ssh](.ssh) provides a private SSH key

## License Scanning
